
let started
let stopped
let price
let totalTime
let totalMin


let start = () =>{
    let day = new Date()
    let time = day.toLocaleTimeString()
   let startTime = time[0] + time[1] + time[2]+ time[3] + " "+ time[8]+ time[9]
   document.getElementById('timeStart').innerHTML = startTime
    started = day.getTime()

   document.getElementById('start').style.display = "none"
   document.getElementById('stop').style.display = "unset"
}

let stop = () =>{
    let day = new Date()
    let time = day.toLocaleTimeString()
    let stopTime = time[0] + time[1] + time[2]+ time[3] + " "+ time[8]+ time[9]
  
    stopped = day.getTime()
   
     totalTime = stopped - started
     totalMin = Math.floor(totalTime/60000)
    document.getElementById('timeStop').innerHTML = stopTime 
    document.getElementById('totalTime').innerHTML = totalMin
    document.getElementById('stop').style.display = "none"
    document.getElementById('clear').style.display = "unset"
    
    if(totalMin >= 0 && totalMin <=15)   
    price= 500
    else if(totalMin > 16 && totalMin <= 30)
    price = 1000
    else if(totalMin >30 && totalMin <= 60)
    price = 1500
    else{
    let totalHour = Math.floor(totalMin/60)
        totalMin = totalMin%60
        if(totalMin == 0)
        price = totalHour * 1500
        else if(totalMin > 0 && totalMin <=15)  
        price = (totalHour * 1500) +  500      
        else if(totalMin > 16 && totalMin <= 30)
        price = (totalHour * 1500) +  1000 
        else
        price = (totalHour * 1500) + 1500
    }
    document.getElementById('pay').innerHTML = price

}

let clearTime = () =>{
   document.getElementById('timeStart').innerHTML = "0:00"
   document.getElementById('timeStop').innerHTML = "0:00"
   document.getElementById('totalTime').innerHTML = 0
   document.getElementById('pay').innerHTML = 0
   document.getElementById('clear').style.display = "none"
   document.getElementById('start').style.display = "unset"
}


let liveTime = setInterval(()=>{
    let day = new Date()
    let date = day.toString().split(" ")
    let getDate = date[0]+" " + date[1]+" " + date[2]+" " + date[3]
    let getTime = day.toLocaleTimeString()
    document.getElementById('liveTime').innerHTML = getDate + " " + getTime
 
},1000)

